package states;

import states.abstractstate.State;
import task.Task;

public class DoneState extends State {

    DoneState(Task task) {
        super(task);
    }

    @Override
    public String done() {
        task.changeState(new DoneState(task));
        return "task.Task " + task.getName() + " is in 'DONE' states.abstractstate.State.";
    }
}
