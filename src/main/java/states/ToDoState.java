package states;


import states.abstractstate.State;
import task.Task;

public class ToDoState extends State {
    public ToDoState(Task task) {
        super(task);
    }

    public String inProgress() {
        task.changeState(new InProgressState(task));
        return "task:" + task.getName() + " goes to PROGRESS states";
    }
}
