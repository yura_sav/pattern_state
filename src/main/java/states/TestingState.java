package states;

import states.abstractstate.State;
import task.Task;

public class TestingState extends State {

    TestingState(Task task) {
        super(task);
    }

    @Override
    public String inProgress() {
        task.changeState(new InProgressState(task));
        return "task.Task: " + task.getName() + " now goes to 'IN PROGRESS' states.abstractstate.State.";
    }

    @Override
    public String done() {
        task.changeState(new DoneState(task));
        return "task.Task: " + task.getName() + " now goes to 'DONE' states.abstractstate.State.";
    }
}
