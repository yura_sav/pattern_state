package states;

import states.abstractstate.State;
import task.Task;

public class InProgressState extends State {
    InProgressState(Task task) {
        super(task);
    }

    @Override
    public String codeReview() {
        task.changeState(new CodeReviewState(task));
        return "task.Task " + task.getName() + " now goes to 'CODE REVIEW' states.abstractstate.State.";
    }

    @Override
    public String toDo() {
        task.changeState(new ToDoState(task));
        return "task.Task " + task.getName() + " now goes to 'TO DO' states.abstractstate.State.";
    }
}
