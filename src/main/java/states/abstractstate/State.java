package states.abstractstate;

import task.Task;

public abstract class State {
   protected Task task;

   public State(Task task) {
        this.task = task;
    }

    public String toDo() {
        return task.getName() + " can not be placed in 'TO DO' section";
    }

    public String inProgress() {
        return task.getName() + " can not be placed in 'In progress' section";
    }

    public String codeReview() {
        return task.getName() + " can not be placed in 'Code Review' section";
    }

    public String testing() {
        return task.getName() + " can not be placed in 'Testing' section";
    }

    public String done() {
        return task.getName() + " can not be placed in 'Done' section";
    }

}
