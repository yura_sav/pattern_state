package states;

import states.abstractstate.State;
import task.Task;

public class CodeReviewState extends State {

    CodeReviewState(Task task) {
        super(task);
    }

    @Override
    public String inProgress() {
        task.changeState(new InProgressState(task));
        return "task.Task " + task.getName() + " now goes to 'IN PROGRESS' states.abstractstate.State.";
    }

    @Override
    public String testing() {
        task.changeState(new TestingState(task));
        return "task.Task " + task.getName() + " now goes to 'TESTING' states.abstractstate.State.";
    }
}
