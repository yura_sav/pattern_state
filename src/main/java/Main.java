import states.DoneState;
import task.Task;

import java.util.*;

public class Main {
    private static int quantity;

    private static int getRandomNumber() {
        Random rand = new Random();
        return rand.nextInt(quantity) + 1;
    }

    public static void main(String[] args) {
        List<Task> tasks = getTasks();
        quantity = tasks.size();
        tasks.forEach(w -> System.out.println(w.inProgress()));

        while (!(tasks.stream()
                .filter(s -> s.getState().getClass().equals(DoneState.class))
                .count() == quantity)) {
            tasks.stream()
                    .filter(s -> !s.getState().getClass().equals(DoneState.class))
                    .forEach(t -> t.choose(getRandomNumber()));
        }
        System.out.println(("\nAFTER:"));
        tasks.forEach(w -> System.out.println(w.done()));
    }

    private static List<Task> getTasks() {
        List<Task> tasks = new LinkedList<>();
        tasks.add(new Task("Authentication"));
        tasks.add(new Task("DataBaseStaff"));
        tasks.add(new Task("FrontEnd"));
        tasks.add(new Task("BackEnd"));
        tasks.add(new Task("UI"));
        return tasks;
    }
}
