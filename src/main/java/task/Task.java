package task;

import states.*;
import states.abstractstate.State;

public class Task {
    private String name;
    private State state;

    public Task(String name) {
        this.name = name;
        state = new ToDoState(this);

    }

    public void changeState(State state) {
        this.state = state;
    }

    public String getName() {
        return name;
    }

    public State getState() {
        return state;
    }

    private String toDo() {
        return state.toDo();
    }

    public String inProgress() {
        return state.inProgress();
    }

    private String codeReview() {
        return state.codeReview();
    }

    private String testing() {
        return state.testing();
    }

    public String done() {
        return state.done();
    }

    public void choose(int number) {
        if (number == 1) {
            System.out.println(toDo());
        }
        if (number == 2) {
            System.out.println(inProgress());
        }
        if (number == 3) {
            System.out.println(codeReview());
        }
        if (number == 4) {
            System.out.println(testing());
        }
        if (number == 5) {
            System.out.println(done());
        }
    }
}

